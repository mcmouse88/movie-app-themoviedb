package com.mcmouse88.filmsapp.data.retrofit

import com.mcmouse88.filmsapp.domain.models.MoviesModel
import retrofit2.Response

class ApiRepository {
    suspend fun getMovie(): Response<MoviesModel> {
        return ApiModel.api.getPopularMovie()
    }
}