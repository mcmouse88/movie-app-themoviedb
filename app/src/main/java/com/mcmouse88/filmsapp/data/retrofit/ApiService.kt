package com.mcmouse88.filmsapp.data.retrofit

import com.mcmouse88.filmsapp.domain.Constants.END_POINT
import com.mcmouse88.filmsapp.domain.models.MoviesModel
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(END_POINT)
    suspend fun getPopularMovie(): Response<MoviesModel>
}