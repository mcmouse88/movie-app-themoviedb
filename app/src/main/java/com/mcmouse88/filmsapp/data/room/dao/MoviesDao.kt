package com.mcmouse88.filmsapp.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mcmouse88.filmsapp.domain.Constants.NAME_TABLE
import com.mcmouse88.filmsapp.domain.models.Result

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertMovie(movie: Result)

    @Delete
    suspend fun deleteMovie(movie: Result)

    @Query("select * from $NAME_TABLE")
    fun getAllMoviesFromDb(): LiveData<List<Result>>
}