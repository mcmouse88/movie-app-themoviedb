package com.mcmouse88.filmsapp.data.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mcmouse88.filmsapp.domain.Constants.NAME_DB
import com.mcmouse88.filmsapp.data.room.dao.MoviesDao
import com.mcmouse88.filmsapp.domain.models.Result

@Database(entities = [Result::class], version = 1)
abstract class MoviesDatabase : RoomDatabase(){

    abstract fun getMovieDao(): MoviesDao

    companion object {

        @Volatile
        private var instance: MoviesDatabase? = null

        fun  getInstance(context: Context): MoviesDatabase {
            return if (instance == null) {
                instance = Room.databaseBuilder(
                    context,
                    MoviesDatabase::class.java,
                    NAME_DB)
                    .fallbackToDestructiveMigration()
                    .build()
                instance as MoviesDatabase
            } else instance as MoviesDatabase
        }
    }
}