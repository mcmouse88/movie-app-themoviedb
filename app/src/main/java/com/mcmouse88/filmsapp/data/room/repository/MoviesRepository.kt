package com.mcmouse88.filmsapp.data.room.repository

import androidx.lifecycle.LiveData
import com.mcmouse88.filmsapp.domain.models.Result

interface MoviesRepository {

    val allMovieFromDb: LiveData<List<Result>>

    suspend fun insertMovie(movie: Result)

    suspend fun deleteMovie(movie: Result)
}