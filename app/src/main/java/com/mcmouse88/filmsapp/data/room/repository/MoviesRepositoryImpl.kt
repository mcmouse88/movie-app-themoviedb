package com.mcmouse88.filmsapp.data.room.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.mcmouse88.filmsapp.data.room.dao.MoviesDao
import com.mcmouse88.filmsapp.data.room.database.MoviesDatabase
import com.mcmouse88.filmsapp.domain.models.Result

class MoviesRepositoryImpl(
    application: Application
    ) : MoviesRepository {

    private val movieDao = MoviesDatabase.getInstance(application).getMovieDao()

    override val allMovieFromDb: LiveData<List<Result>>
        get() = movieDao.getAllMoviesFromDb()

    override suspend fun insertMovie(movie: Result) {
        movieDao.insertMovie(movie)
    }

    override suspend fun deleteMovie(movie: Result) {
        movieDao.deleteMovie(movie)
    }


}