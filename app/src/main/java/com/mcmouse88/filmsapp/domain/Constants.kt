package com.mcmouse88.filmsapp.domain

object Constants {
    const val BASE_URL = "https://api.themoviedb.org"
    const val END_POINT = "/3/movie/popular?api_key=eb502d8228f273b44364afbd864dd0c9&language=en-US&page=1"
    const val BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500"
    const val NAME_TABLE = "table_movie"
    const val NAME_DB = "movie_database"
}