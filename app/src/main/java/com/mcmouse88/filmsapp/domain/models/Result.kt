package com.mcmouse88.filmsapp.domain.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mcmouse88.filmsapp.domain.Constants.NAME_TABLE
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = NAME_TABLE)
data class Result(
    val adult: Boolean,
    val backdrop_path: String,

    // val genre_ids: List<Int>,

    @PrimaryKey
    val id: Int,
    val original_language: String,
    val original_title: String,

    @ColumnInfo
    val overview: String,
    val popularity: Double,

    @ColumnInfo
    val poster_path: String,

    @ColumnInfo
    val release_date: String,

    @ColumnInfo
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int,
) : Parcelable