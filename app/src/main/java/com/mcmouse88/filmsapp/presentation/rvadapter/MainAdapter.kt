package com.mcmouse88.filmsapp.presentation.rvadapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mcmouse88.filmsapp.domain.Constants.BASE_URL_IMAGE
import com.mcmouse88.filmsapp.R
import com.mcmouse88.filmsapp.domain.models.Result

class MainAdapter(
    private val context: Context
) : RecyclerView.Adapter<MainAdapter.MovieViewHolder>() {

    var listMovie = emptyList<Result>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var clickItemMovie: ((Result) -> Unit)? = null

    inner class MovieViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivPoster: ImageView = view.findViewById(R.id.iv_main_poster)
        val tvTitle: TextView = view.findViewById(R.id.tv_main_title)
        val tvDate: TextView = view.findViewById(R.id.tv_main_date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_movie,
            parent,
            false
        )
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = listMovie[position]
        holder.apply {
            tvDate.text = movie.release_date
            tvTitle.text = movie.title
            Glide.with(context).load(BASE_URL_IMAGE + movie.poster_path).centerCrop()
                .placeholder(androidx.appcompat.R.drawable.abc_ab_share_pack_mtrl_alpha)
                .into(holder.ivPoster)
        }
        holder.itemView.setOnClickListener {
            clickItemMovie?.invoke(movie)
        }

    }

    override fun getItemCount(): Int = listMovie.size
}