package com.mcmouse88.filmsapp.presentation.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.mcmouse88.filmsapp.domain.Constants.BASE_URL_IMAGE
import com.mcmouse88.filmsapp.R
import com.mcmouse88.filmsapp.databinding.FragmentDetailBinding
import com.mcmouse88.filmsapp.presentation.viewmodels.DetailViewModel
import com.mcmouse88.filmsapp.domain.models.Result

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding
        get() = _binding ?: throw RuntimeException("FragmentDetailBinding is null")

    private val detailViewModel by lazy {
        ViewModelProvider(this)[DetailViewModel::class.java]
    }

    private val args by navArgs<DetailFragmentArgs>()
    private val currentMovie by lazy {
        args.result
    }

    private var isFavorite: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailInfo()
    }

    private fun detailInfo() {
        binding.apply {
            tvDetailTitle.text = currentMovie.title
            tvDetailDate.text = currentMovie.release_date
            tvDetailDescription.text = currentMovie.overview
            Glide.with(this@DetailFragment)
                .load(BASE_URL_IMAGE + currentMovie.poster_path)
                .placeholder(androidx.appcompat.R.drawable.abc_ab_share_pack_mtrl_alpha)
                .into(ivDetailPoster)
            buttonDetailBack.setOnClickListener { findNavController().popBackStack() }
            buttonDetailAddFavorite.setOnClickListener {
                addToFavorite()
            }
        }
    }

    private fun addToFavorite() {
        if (!isFavorite) {
            binding.buttonDetailAddFavorite
                .setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_favorite,
                    0
                )
            detailViewModel.addToFavorite(currentMovie)

        }
        else {
            binding.buttonDetailAddFavorite
                .setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_favorite_border,
                    0
                )
            detailViewModel.addToFavorite(currentMovie)
        }
        isFavorite = !isFavorite
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}