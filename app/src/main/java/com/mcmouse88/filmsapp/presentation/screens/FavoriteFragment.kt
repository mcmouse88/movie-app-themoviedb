package com.mcmouse88.filmsapp.presentation.screens

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.mcmouse88.filmsapp.databinding.FragmentFavoriteBinding
import com.mcmouse88.filmsapp.presentation.rvadapter.FavoriteAdapter
import com.mcmouse88.filmsapp.presentation.viewmodels.FavoriteFragmentViewModel
import com.mcmouse88.filmsapp.presentation.viewmodels.factory.FavoriteFragmentViewModelFactory

class FavoriteFragment : Fragment() {

    private var _binding: FragmentFavoriteBinding? = null
    private val binding: FragmentFavoriteBinding
        get() = _binding ?: throw RuntimeException("FragmentFavoriteBinding is null")

    private val adapter by lazy {
        context?.let {
            FavoriteAdapter(it)
        } ?: throw RuntimeException("FragmentFavoriteBinding is null")
    }

    private val factory by lazy { FavoriteFragmentViewModelFactory(requireActivity().application) }

    private val favoriteViewModel by lazy {
        ViewModelProvider(this, factory)[FavoriteFragmentViewModel::class.java]
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        favoriteViewModel.getAllMoviesFromDb()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoriteBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val rvMovie = binding.rvFavoriteMovies
        rvMovie.adapter = adapter
        favoriteViewModel.allFavoriteMovie.observe(viewLifecycleOwner) {
            adapter.listFavorites = it
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}