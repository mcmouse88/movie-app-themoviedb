package com.mcmouse88.filmsapp.presentation.screens

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mcmouse88.filmsapp.R
import com.mcmouse88.filmsapp.databinding.FragmentMainBinding
import com.mcmouse88.filmsapp.presentation.rvadapter.MainAdapter
import com.mcmouse88.filmsapp.presentation.viewmodels.MainFragmentViewModel
import com.mcmouse88.filmsapp.presentation.viewmodels.factory.FavoriteFragmentViewModelFactory

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding: FragmentMainBinding
        get() = _binding ?: throw RuntimeException("FragmentMainBinding is null")

    private val adapter by lazy {
        context?.let {
            MainAdapter(it)
        } ?: throw RuntimeException("Adapter in MainFragment is null")
    }

    private val mainViewModel by lazy {
        ViewModelProvider(this)[MainFragmentViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        clickItem()
    }

    private fun setupRecyclerView() {
        val rvMovies = binding.rvMovies
        rvMovies.adapter = adapter
        mainViewModel.movieList.observe(viewLifecycleOwner) {
            adapter.listMovie = it
        }
    }

    private fun clickItem() {
        adapter.clickItemMovie = {
            findNavController().navigate(
                MainFragmentDirections.actionMainFragmentToDetailFragment(it)
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("checkFrag", "onDestroy")
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.item_favorite -> {
                findNavController().navigate(R.id.action_mainFragment_to_favoriteFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mainViewModel.getMovies()
        Log.d("checkFrag", "onAttach")
    }
}