package com.mcmouse88.filmsapp.presentation.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.mcmouse88.filmsapp.data.room.repository.MoviesRepositoryImpl
import com.mcmouse88.filmsapp.domain.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(application: Application) : AndroidViewModel(application) {

    private val dbRepository = MoviesRepositoryImpl(application)

    val allFavoriteMovie = dbRepository.allMovieFromDb

    fun getAllMoviesFromDb() = dbRepository.allMovieFromDb

    fun addToFavorite(movie: Result) {
        viewModelScope.launch(Dispatchers.IO) {
            dbRepository.insertMovie(movie)
        }
    }

    fun deleteToFavorite(movie: Result) {
        viewModelScope.launch(Dispatchers.IO) {
            dbRepository.deleteMovie(movie)
        }
    }
}