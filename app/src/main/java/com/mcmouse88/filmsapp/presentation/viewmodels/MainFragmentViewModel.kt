package com.mcmouse88.filmsapp.presentation.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcmouse88.filmsapp.data.retrofit.ApiRepository
import com.mcmouse88.filmsapp.data.room.database.MoviesDatabase
import com.mcmouse88.filmsapp.data.room.repository.MoviesRepositoryImpl
import com.mcmouse88.filmsapp.domain.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainFragmentViewModel() : ViewModel() {

    private val repository = ApiRepository()

    private val _movieList: MutableLiveData<List<Result>> = MutableLiveData()
    val movieList: LiveData<List<Result>>
        get() = _movieList

    fun getMovies() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getMovie().let {
                if (it.isSuccessful) {
                    _movieList.postValue(it.body()?.results)
                } else {
                    Log.d("checkData", "Fail load data ${it.errorBody()}")
                }
            }
        }
    }
}