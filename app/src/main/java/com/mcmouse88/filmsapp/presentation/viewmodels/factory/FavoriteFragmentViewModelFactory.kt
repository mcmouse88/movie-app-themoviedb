package com.mcmouse88.filmsapp.presentation.viewmodels.factory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mcmouse88.filmsapp.presentation.viewmodels.FavoriteFragmentViewModel

class FavoriteFragmentViewModelFactory(
    private  val application: Application
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoriteFragmentViewModel::class.java)) {
            return FavoriteFragmentViewModel(application) as T
        } else throw RuntimeException("Unknown ViewModel class $modelClass")
    }
}